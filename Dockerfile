# Step 1: build binary
FROM alpine:3.14 AS build
RUN apk update && apk upgrade && apk add --no-cache go
WORKDIR /app
ENV GOPATH /app
RUN go get -u github.com/fimad/ggircd/ggircd
RUN go install github.com/fimad/ggircd/ggircd@latest

# Step 2: deployment image
FROM alpine:3.14
WORKDIR /app
COPY --from=build /app/bin/ggircd /app/bin/ggircd
COPY ggircd.conf /etc/ggircd/ggircd.conf
COPY motd /etc/ggircd/motd

EXPOSE 6667
USER 1001

ENTRYPOINT ["/app/bin/ggircd"]
